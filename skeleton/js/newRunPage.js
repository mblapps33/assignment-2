
// Initializing variables required
var LatLong = [];
var LatLong2 = [];
var Accuracy;
var id, target, options;
var marker = [];
var line= [];
var e;
var rawDist;
var sec = 0
var min=0
var hour=0
var timer=0
var theE = 0;
var runList= []
var key = 'RUN1'
var initialLocation=[];
var finlLocation=[];
var Lat=[];
var distanceCheck;


// function to store the run  
function storeRun()
{
      var startTime = startRun(latlng, randomDestination(latlng))
      var endTime, distanceAB, timeRecord, distance = completeRun(startTime, LatLng, randomDestination(latlng))
      var run = new Run (latlng, randomDestination(latlng), startTime, endTime, distanceAB, timeRecord)
      console.log(run)
      document.getElementById("storeBut").disabled = true;
      document.getElementById("buttonRD").disabled = false;
  }   

// function to get the accuracy and display in html
 function getAccr(){
     document.getElementById("accrLoc").innerHTML = "Accuracy : " + Accuracy + " meter";
     return Accuracy
 }

// function to get current location
 function success(pos) {
     var crd = pos.coords;
     LatLong = [crd.longitude, crd.latitude]
     Accuracy = crd.accuracy;
     return Accuracy
 }

// function to ensure the get random destionation function is activated only if accuracy is below 20 m 
function buttonRD(){
    let Accuracy = 18;  // DISABLE THIS WHEN ACCURACY SIGNAL IS READY
    if(Accuracy<20){
          document.getElementById("genDis").disabled = false;
    } else{
          document.getElementById("genDis").disabled = true;
    }                
}

// function to disable button of each respective function it triggers
 function buttonBR(){
     document.getElementById("begRun").disabled = false;
     setInterval(getDistance(LatLong,LatLong2),1000);;
 }
 
//function to enable stopRun button
 function stopRun(){
     document.getElementById("stpRun").disabled = false;
 }

//function to enable delete location button
 function delL(){
     document.getElementById("deleteLocBut").disabled = false;
 }

// function error corresponding to if success function(line 35) failed 
 function error(err) {
        console.warn('ERROR(' + err.code + '): ' + err.message);
    }
    options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
 };
 
 id = navigator.geolocation.watchPosition(success, error, options);
// function to get random position 
    function randomDestination(LatLong){
  var dist1 = 80 + Math.random()*90;
     var dist2 = 80 + Math.random()*90;
  var angle = Math.random()*2;
  if(angle<0.5){
  	   LatLong2[0] = LatLong[0] + dist1 * 0.001/150;
  	   LatLong2[1] = LatLong[1] + dist2 * 0.001/150;
  } else if (angle>=0.5 && angle<1){
  	   LatLong2[0] = LatLong[0] - dist1 * 0.001/150;
  	   LatLong2[1] = LatLong[1] + dist2 * 0.001/150;
  } else if (angle>=1 && angle<1.5){
  	   LatLong2[0] = LatLong[0] - dist1 * 0.001/150;
  	   LatLong2[1] = LatLong[1] - dist2 * 0.001/150;
  } else {
  	   LatLong2[0] = LatLong[0] + dist1 * 0.001/150;
  	   LatLong2[1] = LatLong[1] - dist2 * 0.001/150;
  }
       marker = new mapboxgl.Marker({
           color: "red"
       });
       // syntax to get marker of user location and destination position linked by a line
       marker.setLngLat(LatLong2);
       marker.addTo(map);
       
       map.addLayer({
                  "id": "route",
                  "type": "line",
                  "source": {
                      "type": "geojson",
                      "data": {
                          "type": "Feature",
                          "properties": {},
                          "geometry": {
                              "type": "LineString",
                              "coordinates": [
                                  [LatLong[0], LatLong[1]],
                                  [ LatLong2[0], LatLong2[1]]
                                  
                              ]
                          }
                      }
                  },
                  "layout": {
                      "line-join": "round",
                      "line-cap": "round"
                  },
                  "paint": {
                      "line-color": "#888",
                      "line-width": 8
                  }
              });
              } 

// function to delete previously random generated destination 
function delLoc(){
    marker.remove()
    map.removeLayer('route')
    map.removeSource('route')
}

// function to get the distance between user and destination 
function getDistance(a,b){
        var R = 6371; // Radius of the earth in km
          var dLat = deg2rad(b[1]-a[1]);  // deg2rad below
          var dLon = deg2rad(b[0]-a[0]); 
          var c = 
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(deg2rad(a[1])) * Math.cos(deg2rad(b[1])) * 
            Math.sin(dLon/2) * Math.sin(dLon/2)
            ; 
          var d = 2 * Math.atan2(Math.sqrt(c), Math.sqrt(1-c)); 
           e = R * d*1000; // Distance in km
    return Math.abs(e)
    document.getElementById("distCal").innerHTML = "Distance to destination: "+ e + " meter";
    }

//function to convert degree to radian
 function deg2rad(deg) {
  return deg * (Math.PI/180)
}


// function to begin timer and distance calculator 
function beginRun(theTime){ 
      if (typeof LatLong2[1]==="number"){
    	do{
          var startingLoc=[LatLong[0],LatLong[1]]
    	var finalLoc = [LatLong2[0],LatLong2[1]]
    	timer = setInterval(theTime,1000)
          function theTime() {
    sec++ 
    if(sec===60){
    	sec=0
    	min++
    	if(min===60){
    		min=0
    		hr++
    	}
    }
              document.getElementById("timer").innerHTML = "Time  "+ hour + " : " + min + " : " + sec}
    	}while(e<10)
      }else{console.log(false)}
}
    
// function to store initial distance between langLat and langLat2
 function setConstE(){
     theE = e;
 }
    
// function to stop run 
 function myStopFunction() {
     endTime=[hour,min,sec]
     clearInterval(timer);
     runTime= endTime[0] + ":" + endTime[1]+":"+endTime[2]
     return endTime
 }
    
// function to enable saveRun button
 function saveRB(){
         document.getElementById("saveRun").disabled = false;
 }
  
//function to generate and siplay dynamic random destination
 function dyDis(){
    setInterval(function dynamicDesti(){
           getDistance(LatLong,LatLong2);
            document.getElementById("distCalRem").innerHTML = "Distance remaining: "+ e + " meter";
        },1000)
 }
    
 // function to save run 
 function saveRun(){
        doc = prompt("Please Enter Run Name "); 
        if (doc != null) { 
            console.log(doc)
        }         
        setClass.runName = doc
        setClass.initLoc = [LatLong[0],LatLong[1]]
        setClass.finLoc = [LatLong2[0],LatLong2[1]]
        if (typeof(Storage) !== "undefined")
        {
            
            var stringifyRunTnstance = JSON.stringify(setClass)
            if  (localStorage.getItem("RUN1")== null){
                runList.push(setClass)
                localStorage.setItem(key, JSON.stringify(runList))
            }
            else{
                var followingListarray = JSON.parse(localStorage.getItem("RUN1"))
                followingListarray.push(setClass)
                localStorage.setItem(key, JSON.stringify(followingListarray))
            }
            
        }
        else
        {
            console.log("Error: localStorage is not supported by current browser.");
        }
        
        
        
        
}

// function to control newRun.html behaviour 
function keyInsertInstance(){
            var runIndex = localStorage.getItem(APP_PREFIX + "-selectedRun");
                    if (runIndex !== null)
                    {
                        // If a run index was specified, show name in header bar title. This
                        // is just to demonstrate navigation.  You should set the page header bar
                        // title to an appropriate description of the run being displayed.
                        var runNames = [ "Run1", "Run B" ];
                        document.getElementById("headerBarTitle").textContent = runNames[1];
                    }
            
            var pointObj = new Run();
            let thisRunList = document.getElementById('viewRun');
            var thisRun = JSON.parse(localStorage.getItem("RUN1"))
            pointObj.convertToPDO(thisRun[runIndex])
            initialLocation = pointObj._newInitLoc
            finlLocation = pointObj._newFinLoc
            map.panTo(initialLocation)
            
            markerKey1 = new mapboxgl.Marker({
                            color: "blue"
            });
    
            markerKey2 = new mapboxgl.Marker({
                            color: "red"
            });
                        
            markerKey1.setLngLat(initialLocation);
            markerKey1.addTo(map);
            markerKey2.setLngLat(finlLocation);
            markerKey2.addTo(map);
    
            map.on('load', function () {
                 map.addLayer({
                     "id": "route",
                     "type": "line",
                     "source": {
                         "type": "geojson",
                         "data": {
                             "type": "Feature",
                             "properties": {},
                             "geometry": {
                                 "type": "LineString",
                                 "coordinates": [
                                     [initialLocation[0], initialLocation[1]],
                                     [finlLocation[0], finlLocation[1]]
                                     
                                 ]
                             }
                         }
                     },
                     "layout": {
                         "line-join": "round",
                         "line-cap": "round"
                     },
                     "paint": {
                         "line-color": "#888",
                         "line-width": 8
                     }
                 });
        });
            document.getElementById("accBut").disabled = true
            document.getElementById("saveRun").disabled = true
            document.getElementById("cR").disabled = false
}
    
// function to calculate distance between tracked new location and the intial saved location
function checkDistanceAR(){
    getDistance(LatLong,initialLocation)
    distanceCheck = e;
    return distanceCheck
}
            
// function to check whether the tracked new location is within 10 meters radius from the inial saved location
function proceed(){
    if(distanceCheck<=20){
        buttonBR()
        LatLong2 = finlLocation
        getDistance(LatLong,LatLong2)
        theE = e;
    }
}
    
// function to remove run object from local storage
function removeKey(){
    localStorage.removeItem("KeyToPan")
}
            
            
            

