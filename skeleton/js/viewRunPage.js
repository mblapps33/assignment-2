// Code for the View Run page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

// Retrieving data from local storage and set to a value storage
// And, intializing variables as storages
var runIndex = localStorage.getItem(APP_PREFIX + "-selectedRun");
var runObject = new Run();
let thisRunList = document.getElementById('viewRun');
var thisRun = JSON.parse(localStorage.getItem("RUN1"));
runObject.convertToPDO(thisRun[runIndex])
var initialLocation = runObject._newInitLoc
var finlLocation = runObject._newFinLoc
var runName = runObject._runName
var disTrav = runObject._distance
var timeTrav = runObject._runTime
var timeInScnd = timeTrav[0]*3600 + timeTrav[1]*60 + timeTrav[2]
var avgSpeed = disTrav/timeInScnd;
var avgS = avgSpeed.toFixed(2)
var disT = disTrav.toFixed(2)
var markerPath = [];
var LatLongPath1 = [initialLocation[0], initialLocation[1]];
var LatLongPath2 = [finlLocation[0], finlLocation[1]];

// displaying Distance travelled and Average speed in the html
document.getElementById("diT").innerHTML = "Distance Travelled: "+ disT + " meter";
document.getElementById("avg").innerHTML = "Average Speed : "+ avgS + " m/s";

// if condition to display the name of the run chosen
if (runIndex !== null)
    {
        // If a run index was specified, show name in header bar title. This
        // is just to demonstrate navigation.  You should set the page header bar
        // title to an appropriate description of the run being displayed.
        document.getElementById("headerBarTitle").textContent = runName;
    }

// function to diplsay line of path taken
function showPath()
      {
          let currentLoc = new mapboxgl.LngLat(initialLocation[0], initialLocation[1])
          map.panTo(currentLoc);
          markerPath[0] = new mapboxgl.Marker({
              color: "blue"
          });
          markerPath[0].setLngLat(LatLongPath1);
          markerPath[0].addTo(map);
          
          markerPath[1] = new mapboxgl.Marker({
              color: "red"
          });
          markerPath[1].setLngLat(LatLongPath2);
          markerPath[1].addTo(map);
          map.addLayer({
              "id": "route",
              "type": "line",
              "source": {
                  "type": "geojson",
                  "data": {
                      "type": "Feature",
                      "properties": {},
                      "geometry": {
                          "type": "LineString",
                          "coordinates": [
                              [initialLocation[0], initialLocation[1]],
                              [finlLocation[0], finlLocation[1]]
                              
                          ]
                      }
                  }
              },
              "layout": {
                  "line-join": "round",
                  "line-cap": "round"
              },
              "paint": {
                  "line-color": "#888",
                  "line-width": 8
              }
          });
      }

// Function to delete the chosen run
function spliceRun() {
  thisRun.splice(runIndex,1);
    localStorage.setItem("RUN1", JSON.stringify(thisRun))
    location.href = "index.html"
}

// funtion to reattempt run, to auto reload to newRun.html & set a key to control newRun.html
function reAttemptRun(){
    location.href="newRun.html"
    localStorage.setItem("KeyToPan", "10")
}

//function to remove the key to control newRun.html

function removeKey(){
    localStorage.removeItem("KeyToPan")
}



        
    


