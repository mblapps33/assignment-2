// Code for the main app page (Past Runs list).

// Function to view the runs in list form
function viewRun(runIndex)
{
    // Save the desired run to local storage so it can be accessed from View Run page.
    localStorage.setItem(APP_PREFIX + "-selectedRun", runIndex);
    // ... and load the View Run page.
    location.href = 'viewRun.html';
}

// retrieving RUN1 data from local storage and creating variables to store data
var runListElement = document.getElementById('runlist');
var runList = JSON.parse(localStorage.getItem("RUN1"))
var runObject = new Run();
let listHTML = "";
var runName="";
let indexing


// for loop to make the run list
for(let i = 0; i<runList.length ; i++){

    runObject.convertToPDO(runList[i])

    runName = runObject.runName
    var distance = runObject._finalDistance
    var runTime = runObject._runTime
    var disTance = Math.abs(distance.toFixed(0))

    listHTML += "<tr> <td onmousedown=\"viewRun("+i+")\" class=\"full-width mdl-data-table__cell--non-numeric\">" + runName ;

    listHTML += "<div class=\"subtitle\">" + " Run time : " + runTime[0] + " hr "+ runTime[1] + " min "+runTime[2] + " sec "+ "|| Distance travelled: " + disTance +" meter  </div></td></tr>";

            runListElement.innerHTML = listHTML;

        }