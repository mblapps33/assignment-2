// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];
var aLoc = [];


// creating Run class
    class Run {
           // Private attributes
           constructor(dist, runT, initDis, remDis,finalDist){
               this._distance = dist; // Distance travelled
               this._initDis = initDis; // initial location
               this._remainDis = remDis; // final location
               this._runTime = runT; // The date the run is started
               this._finalDis = finalDist; // Final distance 
           }
           
           // Public Methods
          
           // Getter
          get finalDistance(){
              this._finalDistance = (this._initDis) - (this._remainDis);
              return this._finalDistance;
          }
  
          get distance(){
               return this._distance;
          }
           get initLoc(){
               return this._initLoc
           }
           get finLoc(){
               return this._finLoc
           }
    
            get initDis(){
                return this._initDis;
            }
    
            get remainDis(){
                return this._remainDis;
            }
            
            get runTime (){
                return this._runTime;
            }
            get runName(){
                return this._runName
            }
  
            // Setter
  
            set newFinalDistance(newFD){
                this._newfinalDistance = (this._initDis) - (this._remainDis);
            }
            
            set distance(newDistance){
                this._newDistance = newDistance;
            }
            set initLoc(newIL){
                this._newInitLoc = newIL
            }
            set finLoc(newFL){
                this._newFinLoc = newFL
            }
            set newRunTime(newRT){
                this._newRunTime = newRT;
            }
            
            set newInitDis(newID){
                this._newInitDis = newID;
            }
            
            set newRemainDis(newRD){
                this._newRemainDis = newRD;
            }
            set runName(newRunName){
                this._runName=newRunName
            }
            
          // class method to calculate distance travelled
            distanceTravelled(){
                this._finalDistance = (this._initDis) - (this._remainDis);
                document.getElementById("distTrav").innerHTML = "Distance travelled: "+ this._finalDistance + " meter";
            }
          
          // converting class method so it can be used publicly
            convertToPDO(obj){
                this._distance = obj._distance;
                this._runTime = obj._runTime;
                this._initDis = obj._initDis;
                this._remainDis = obj._remainDis;
                this._runName=obj._runName
                this._initLoc=obj._initLoc
                this._finLoc=obj._finLoc
                this._newInitLoc=obj._newInitLoc
                this._newFinLoc=obj._newFinLoc
                this._finalDistance = obj._finalDistance
                }
            
  }

            
            var setClass;
            
            // function to store data into the Run class
            function setClassVal(theE,endTime,theE,e){         
                setClass = new Run(theE, endTime, theE, e);
                document.getElementById("runTime").innerHTML = "Time taken: "+ setClass._runTime[0] + " hour "+ setClass._runTime[1] + " min " +setClass._runTime[2]+ " second ";
                setClass.distanceTravelled()
            }

